import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-jonas',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './jonas.component.html',
  styleUrl: './jonas.component.css'
})
export class JonasComponent {

}
