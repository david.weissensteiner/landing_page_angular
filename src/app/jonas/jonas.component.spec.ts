import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JonasComponent } from './jonas.component';

describe('JonasComponent', () => {
  let component: JonasComponent;
  let fixture: ComponentFixture<JonasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [JonasComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(JonasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
